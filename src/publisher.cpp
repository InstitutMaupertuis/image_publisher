#include <cv_bridge/cv_bridge.h>
#include <image_transport/image_transport.h>
#include <memory>
#include <opencv2/highgui/highgui.hpp>
#include <ros/package.h>
#include <ros/ros.h>
#include <sstream>

std::unique_ptr<ros::NodeHandle> nh;
std::unique_ptr<image_transport::Publisher> pub;

unsigned webcam(const unsigned video_source = 0)
{
  // Open video source
  cv::VideoCapture cap(video_source);
  if (!cap.isOpened())
  {
    ROS_ERROR_STREAM("Could not open video source");
    return 1;
  }

  cap.set(CV_CAP_PROP_FRAME_WIDTH, 1280);
  cap.set(CV_CAP_PROP_FRAME_HEIGHT, 720);

  // frame will contain the image fetched by OpenCV
  cv::Mat frame;
  // msg will contain the converted frame to be sent on the ROS topics
  sensor_msgs::ImagePtr msg;

  cap >> frame;
  if (!frame.empty())
    ROS_INFO_STREAM("Image width = " << frame.cols << ", height = " << frame.rows);

  // Fetch OpenCV image, convert to ROS message, publish
  ros::Rate loop_rate(30);
  while (nh->ok())
  {
    // Fetch image
    cap >> frame;
    if (frame.empty())
    {
      ROS_ERROR_STREAM("Frame is empty");
      ros::Duration(0.5).sleep();
      continue;
    }

    // Convert OpenCV image to ROS message
    msg = cv_bridge::CvImage(std_msgs::Header(), "bgr8", frame).toImageMsg();
    // Publish image
    pub->publish(msg);
    // Process callbacks (http://wiki.ros.org/roscpp/Overview/Callbacks%20and%20Spinning)
    ros::spinOnce();
    // Ensures we fetch 30 images per second
    loop_rate.sleep();
  }

  return 0;
}

unsigned staticImage(std::string &image)
{
  if (image.empty())
    image = "image.png";

  const std::string package(ros::package::getPath("image_publisher"));
  cv::Mat frame = cv::imread(package + "/media/" + image, CV_LOAD_IMAGE_UNCHANGED);
  ROS_INFO_STREAM("Image path = " << package << "/media/" << image << std::endl <<
                  "Image width = " << frame.cols << ", height = " << frame.rows << std::endl <<
                  "Image channels = " << frame.channels());

  if (frame.empty())
  {
    ROS_ERROR_STREAM("Empty frame");
    return 1;
  }

  sensor_msgs::ImagePtr msg;
  if (frame.channels() == 1)
    msg = cv_bridge::CvImage(std_msgs::Header(), "mono8", frame).toImageMsg();
  else if (frame.channels() == 3)
    msg = cv_bridge::CvImage(std_msgs::Header(), "bgr8", frame).toImageMsg();
  else
  {
    ROS_ERROR_STREAM("Frame has unsupported number of channels (" << frame.channels() << ")");
    return 1;
  }

  ros::Rate loop_rate(5);
  while (nh->ok())
  {
    pub->publish(msg);
    ros::spinOnce();
    loop_rate.sleep();
  }

  return 0;
}

int main(int argc,
         char** argv)
{
  ros::init(argc, argv, "image_publisher");
  nh.reset(new ros::NodeHandle("~"));

  std::string topic;
  nh->getParam("topic", topic);
  if (topic.empty())
    topic = "image";

  image_transport::ImageTransport it(*nh);
  pub.reset(new image_transport::Publisher);
  *pub = it.advertise(topic, 1);

  std::string image;
  nh->getParam("image", image);

  if (nh->param("static_image", false) == true)
  {
    ROS_INFO_STREAM("Publishing a static image");
    return staticImage(image);
  }
  else
  {
    ROS_INFO_STREAM("Publishing webcam image");
    return webcam(0);
  }
}
