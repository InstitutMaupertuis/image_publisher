[![Institut Maupertuis logo](http://www.institutmaupertuis.fr/media/gabarit/logo.png)](http://www.institutmaupertuis.fr)

# Usage
```bash
rosrun image_publisher publisher _topic:=webcam
```
This will broadcast the first webcam image found

To publish a static image instead of the webcam use:
```bash
rosrun image_publisher publisher __name:=grayscale _static_image:=true _image:=grayscale.jpg _topic:=grayscale
rosrun image_publisher publisher __name:=color _static_image:=true _image:=color.jpg _topic:=color
```
